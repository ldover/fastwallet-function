import corsLib from 'cors';
// const ec = require("elliptic").ec;
const sha256 = require("js-sha256");
const ripemd160 = require("ripemd160");
const base58 = require("bs58");
const bip39 = require("bip39");
const hdkey = require("hdkey");

// const ecdsa = new ec("secp256k1");

const cors = corsLib({
  origin: true,
});


export default function (req, res) {
  cors(req, res, async () => {

    if (req.method !== 'POST') {
      return res.status(405).end();
    }

    try {
      const body = req.body;
      const numbers = body.numbers;

      if (!numbers || !numbers.length) {
        return res.status(400).json({
          status: 'error',
          message: 'Request must contain `numbers` parameter.'
        })
      }

      const binary = (hex) => Buffer.from(hex, "hex");

      /**
       *
       * @param numbers - Array of booleans, EX: [true, false, true, false, ...]
       * @returns {string}
       */
      function getBitsFromNumbers(numbers) {
        let bits = '';
        numbers.slice(0, 256).map(number => bits += number ? '1' : '0');
        return bits;
      }

      let bits = getBitsFromNumbers(numbers)

      let seed;
      const max = binary(
        "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEBAAEDCE6AF48A03BBFD25E8CD0364140"
      );

      // TODO: Do we have to add back this check for
      //   if (Buffer.compare(max, seed) === 1) {

      let bytes = []
      for (let i = 0; i < 256; i += 8) {
        let byte = parseInt(bits.slice(i, i + 8), 2);
        bytes.push(byte)
      }

      seed = Buffer.from(new Uint8Array(bytes).buffer);

      const mnemonic = bip39.entropyToMnemonic(seed.toString("hex"));

      console.log("Seed: ", seed.toString("hex"));
      console.log("Mnemonic: ", mnemonic);

      /*
       *
       * Generate HD Keys
       *
       */

      const hdKey = hdkey.fromMasterSeed(seed);

      console.log("Master Private Key: " + hdKey.privateExtendedKey);
      console.log("Master Public Key: " + hdKey.publicExtendedKey);

      const getChecksum = (prefixedAddress) => {
        // Create SHA256 hash of addressData
        const sha1 = sha256(binary(prefixedAddress));

        // Create SHA256 hash of SHA
        const sha2 = sha256(binary(sha1));

        // Save the 1st byte of step 3 - save as "checksum"
        return sha2.substring(0, 8);
      };

      const validateAddress = (address) => {
        const decoded = Buffer.from(base58.decode(address)).toString("hex");

        const checksum = decoded.substring(decoded.length - 8);
        const prefixedAddress = decoded.substring(0, decoded.length - 8);

        return checksum === getChecksum(prefixedAddress);
      };

      const getAddress = function (change, index) {
        // m / purpose' / coin_type' / account' / change / address_index
        const {privateKey, publicKey} = hdKey.derive(
          `m/44'/0'/0'/${change ? 1 : 0}/${index}`
        );

        // Run public key through SHA
        let shaHash = sha256(publicKey);

        // Run SHA through RIPEMD160
        let publicKeyHash = new ripemd160()
          .update(binary(shaHash))
          .digest()
          .toString("hex");

        // Define prefix for address
        const prefix = "00";

        // Get Checksum
        const checksum = getChecksum(prefix + publicKeyHash);

        // Combine prefix public hash and checksum
        const finalAddress = prefix + publicKeyHash + checksum;

        // Save Bas58 Encoding for final public key
        const base58Address = base58.encode(binary(finalAddress));

        console.log(
          "Public Address: ",
          base58Address,
          "Valid: ",
          validateAddress(base58Address)
        );

        return base58Address;
      };

      const address1 = getAddress(false, 0);
      const address2 = getAddress(false, 1);
      const address3 = getAddress(false, 2);

      return res.status(200).json({
        status: 'success',
        mnemonic,
        addresses: [address1, address2, address3],
      });


    } catch (err) {
      return res.status(500).json({
        status: 'error',
        message: JSON.stringify(err)
      });
    }
  })
}
